package taller.interfaz;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import taller.mundo.Jugador;
import taller.mundo.LineaCuatro;

public class Interfaz 
{
	/**
	 * Juego actual
	 */
	private LineaCuatro juego;

	/**
	 * Escáner de texto ingresado por el usuario en la consola
	 */
	private Scanner sc;

	/**
	 * Crea una nueva instancia de la clase de interacción del usuario con la consola
	 */
	public Interfaz()
	{
		sc= new Scanner (System.in);
		while (true)
		{
			imprimirMenu();
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					empezarJuego();
				}
				else if(opt==2)
				{
					empezarJuegoMaquina();
				}
				else if(opt==3)
				{
					System.out.println("¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando inválido");
				continue;
			}


		}
	}
	/**
	 * Imprime el menú principal
	 */
	public void imprimirMenu()
	{
		System.out.println("------------------------LÍNEA CUATRO------------------------");
		System.out.println("-Menú principal-");
		System.out.println("Ingrese un comando:");
		System.out.println("1. Empezar juego con amigos");
		System.out.println("2. Empezar juego vs máquina");
		System.out.println("3. Salir");		
	}

	/**
	 * Crea un nuevo juego
	 */
	public void empezarJuego()
	{
		System.out.println("Introducir los nombres de los jugadores junto con sus simbolos, separados con una coma (,),");
		System.out.println("Nota: El símbolo sólo puede ser de un caractér.");
		System.out.println("Inroduzca END para avanzar al siguiente paso");
		ArrayList <Jugador> jugadores = new ArrayList<Jugador>();
		boolean registro = true;
		while(registro == true)
		{
			String entrada = sc.next();
			if(entrada.equals("END"))
			{
				if(!jugadores.isEmpty())
				{
					registro = false;
					System.out.println("Jugadores registrados: ");
					for(int i = 0; i<jugadores.size(); i++)
					{
						System.out.println(jugadores.get(i).darNombre() + "," + jugadores.get(i).darSimbolo());
					}
				}
				else
				{
					System.out.println("No hay jugadores registrados.");
				}
			}
			else if((entrada.contains(",")) && (entrada != null))
			{
				try
				{
					String[] nomYSymb = entrada.split(",");
					boolean existe = false;
					for(int i = 0; i < jugadores.size(); i++)
					{
						if((jugadores.get(i).darNombre().equals(nomYSymb[0])) || (jugadores.get(i).darSimbolo().equals("(" + nomYSymb[1] + ")")))
						{
							existe = true;
						}
					}
					if(!existe)
					{
						if(nomYSymb[1].length() != 1)
						{
							System.out.println("El tamaño del símbolo es incorrecto, intente de nuevo.");
						}
						else
						{
							Jugador nuevo = new Jugador(nomYSymb[0], "(" + nomYSymb[1] + ")");
							jugadores.add(nuevo);
							System.out.println("Registrado " + nomYSymb[0]);
							System.out.println("Símbolo: " + nomYSymb[1]);
						}
					}
					else
					{
						System.out.println("El nombre O símbolo ya están registrados, por favor intente de nuevo.");
					}

				}
				catch (Exception e)
				{
					for(int i = 0; i<jugadores.size(); i++)
					{
						System.out.println(jugadores.get(i).darNombre() + "\n" +jugadores.get(i).darSimbolo());
					}
					continue;
				}
			}
			else
			{
				System.out.println("por favor intentelo de nuevo.");
			}

		}

		boolean estado2 = false;
		int objetivo = 4;
		while(!estado2)
		{
			try
			{
				System.out.println("Por favor introduzca el número mínimo de fichas con las que se puede alcanzar la victoria.");
				String entrada = sc.next();
				if(Integer.parseInt(entrada)> 0)
				{
					objetivo = Integer.parseInt(entrada);
					estado2 = true;
				}
				else
				{
					System.out.println("El valor introducido es inválido.");
				}
			}
			catch (Exception e)
			{
				System.out.println("El valor introducido es inválido.");
				continue;
			}
		}
		boolean estadoTablero = false;
		while(!estadoTablero)
		{
			try
			{
				System.out.println("Por favor introduzca el tamaño del tablero, columnas primero, filas después. (x,y)");
				String entrada = sc.next();
				String[] entDividida = entrada.split(",");
				int pFil = Integer.parseInt(entDividida[1]);
				int pCol = Integer.parseInt(entDividida[0]);
				juego = new LineaCuatro(jugadores, pFil, pCol, objetivo);
				juego();
			}
			catch (Exception e)
			{
				System.out.println("Error, datos introducidos inválidos: " + e.getMessage());
				continue;
			}
		}
		//TODO
	}

	/**
	 * Modera el juego entre jugadores
	 */
//	NOTA DEL DESARROLLADOR: Debido a que el método terminar() de la clase LineaCuatro
//	se encuentra incompleto, el juego no tiene como terminar.
	public void juego()
	{
		while(juego.fin() == false)
		{
			System.out.println("Turno de: " + juego.darAtacante());
			System.out.println("N° Turno: " + juego.darTurno());
			System.out.println("Introducir número de la columna");
			int col = 0;
			String entrada = sc.next();
			try
			{
				col = Integer.parseInt(entrada);
				juego.registrarJugada(col-1);
				imprimirTablero();
				System.out.println(juego.fin());
				if(juego.fin()== true)
				{
					break;
				}
			}
			catch(Exception e)
			{
				System.out.println("Comando Inválido: " + e.getMessage());
				continue;
			}
		}
		if(juego.fin())
		{
			System.out.println("El ganador es: " + juego.darAtacante());
			boolean control = false;
			while(!control)
			{
				System.out.println("Por favor elija una de las opciones: ");
				System.out.println("1. Iniciar una nueva partida multijugador.");
				System.out.println("2. Iniciar una nueva partida contra la máquina.");
				System.out.println("3. Terminar");
				try
				{
					int opt=Integer.parseInt(sc.next());
					if(opt==1)
					{
						empezarJuego();
					}
					else if(opt==2)
					{
						empezarJuegoMaquina();
					}
					else if(opt==3)
					{
						System.out.println("¡Vuelva pronto!");
						break;
					}
					else
					{
						System.out.println("Comando inválido");
						continue;
					}
				}
				catch (NumberFormatException e)
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
		}

		//TODO
	}
	/**
	 * Empieza el juego contra la máquina
	 */
	public void empezarJuegoMaquina()
	{
		System.out.println("Introducir los nombres de los jugadores junto con sus simbolos, separados con una coma (,),");
		System.out.println("Nota: El símbolo sólo puede ser de un caractér.");
		System.out.println("Este modo solo permite un jugador");
		System.out.println("Si no introduce ningún nombre, solo jugadores de computador jugarán.");
		System.out.println("Inroduzca END para avanzar al siguiente paso");
		ArrayList <Jugador> jugadores = new ArrayList<Jugador>();
		boolean registro = true;
		while(registro == true)
		{
			String entrada = sc.next();
			if(entrada.equals("END"))
			{
				registro = false;
				System.out.println("Jugadores registrados: ");
				for(int i = 0; i<jugadores.size(); i++)
				{
					System.out.println(jugadores.get(i).darNombre() + "," + jugadores.get(i).darSimbolo());
				}
			}
			else if((entrada.contains(",")) && (entrada != null))
			{
				try
				{
					String[] nomYSymb = entrada.split(",");
					boolean existe = false;
					for(int i = 0; i < jugadores.size(); i++)
					{
						if((jugadores.get(i).darNombre().equals(nomYSymb[0])) || (jugadores.get(i).darSimbolo().equals("(" + nomYSymb[1] + ")")))
						{
							existe = true;
						}
					}
					if(!existe)
					{
						if(nomYSymb[1].length() != 1)
						{
							System.out.println("El tamaño del símbolo es incorrecto, intente de nuevo.");
						}
						else
						{
							Jugador nuevo = new Jugador(nomYSymb[0], "(" + nomYSymb[1] + ")");
							jugadores.add(nuevo);
							System.out.println("Registrado " + nomYSymb[0]);
							System.out.println("Símbolo: " + nomYSymb[1]);
						}
					}
					else
					{
						System.out.println("El nombre O símbolo ya están registrados, por favor intente de nuevo.");
					}

				}
				catch (Exception e)
				{
					for(int i = 0; i<jugadores.size(); i++)
					{
						System.out.println(jugadores.get(i).darNombre() + "\n" +jugadores.get(i).darSimbolo());
					}
					continue;
				}
			}
			else
			{
				System.out.println("por favor intentelo de nuevo.");
			}
		}	
		System.out.println("Ahora introduzca la cantidad de AI con las que desea competir (nota: si pone demasiadas, el juego puede ser imposible de ganar)");
		String entrada = sc.next();
		int cantidadAI = Integer.parseInt(entrada);
		int j = 0;
		while(j < cantidadAI)
		{
			Random generador = new Random();
			int ID = generador.nextInt(999);
			int Symb = generador.nextInt(999);
			String nombre = LineaCuatro.COMPUTADORA + ID;
			String simbolo = Integer.toString(Symb); 
			boolean existe = false;
			for(int i = 0; i < jugadores.size(); i++)
			{
				if((jugadores.get(i).darNombre().equals(nombre)) || (jugadores.get(i).darSimbolo().equals("(" + simbolo + ")")))
				{
					existe = true;
				}
			}
			if(!existe)
			{
				Jugador nuevo = new Jugador(nombre, "(" + simbolo + ")");
				jugadores.add(nuevo);
				System.out.println("Registrado " + nombre);
				System.out.println("Símbolo: " + simbolo);
				existe = true;
			}
			else
			{
				j--;
			}
			j++;
		}
		System.out.println("Jugadores: ");
		for(int i = 0; i<jugadores.size(); i++)
		{
			System.out.println(jugadores.get(i).darNombre() + jugadores.get(i).darSimbolo());
		}

		boolean estado2 = false;
		int objetivo = 4;
		while(!estado2)
		{
			try
			{
				System.out.println("Por favor introduzca el número mínimo de fichas con las que se puede alcanzar la victoria.");
				String entrada3 = sc.next();
				if(Integer.parseInt(entrada3)> 0)
				{
					objetivo = Integer.parseInt(entrada3);
					estado2 = true;
				}
				else
				{
					System.out.println("El valor introducido es inválido.");
				}
			}
			catch (Exception e)
			{
				System.out.println("El valor introducido es inválido.");
				continue;
			}
		}
		boolean estadoTablero = false;
		while(!estadoTablero)
		{
			try
			{
				System.out.println("Por favor introduzca el tamaño del tablero, columnas primero, filas después. (x,y)");
				String entrada4 = sc.next();
				String[] entDividida = entrada4.split(",");
				int pFil = Integer.parseInt(entDividida[1]);
				int pCol = Integer.parseInt(entDividida[0]);
				juego = new LineaCuatro(jugadores, pFil, pCol, objetivo);
				juego();
			}
			catch (Exception e)
			{
				System.out.println("Error, datos introducidos inválidos: " + e.getMessage());
				continue;
			}
		}
		//TODO
	}
	/**
	 * Modera el juego contra la máquina
	 */
	public void juegoMaquina()
	{
		while(!juego.fin())
		{
			try
			{
				if(!juego.darAtacante().startsWith(LineaCuatro.COMPUTADORA))
				{
					System.out.println("Turno de: " + juego.darAtacante());
					System.out.println("N° Turno: " + juego.darTurno());
					System.out.println("Introducir número de la columna");
					int col = 0;
					String entrada = sc.next();
					col = Integer.parseInt(entrada);
					juego.registrarJugada(col-1);
					imprimirTablero();
				}
				else
				{
					System.out.println("Turno de: " + juego.darAtacante());
					System.out.println("N° Turno: " + juego.darTurno());
					juego.registrarJugadaAleatoria();
					imprimirTablero();
				}
			}
			catch(Exception e)
			{
				System.out.println("Comando Inválido: " + e.getMessage());
				continue;
			}
		}
		if(juego.fin())
		{
			System.out.println("El ganador es: " + juego.darAtacante());
			boolean control = false;
			while(!control)
			{
				System.out.println("Por favor elija una de las opciones: ");
				System.out.println("1. Iniciar una nueva partida multijugador.");
				System.out.println("2. Iniciar una nueva partida contra la máquina.");
				System.out.println("3. Terminar");
				try
				{
					int opt=Integer.parseInt(sc.next());
					if(opt==1)
					{
						empezarJuego();
					}
					else if(opt==2)
					{
						empezarJuegoMaquina();
					}
					else if(opt==3)
					{
						System.out.println("¡Vuelva pronto!");
						break;
					}
					else
					{
						System.out.println("Comando inválido");
						continue;
					}
				}
				catch (NumberFormatException e)
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
		}
		//TODO
	}

	/**
	 * Imprime el estado actual del juego
	 */
	public void imprimirTablero()
	{
		for(int y = 0; y < juego.darFilas(); y++)
		{
			String fila = "";
			for(int x = 0; x < juego.darColumnas(); x++)
			{
				fila += juego.darTablero()[y][x].darSimbolo();
			}
			System.out.println(fila);
		}
		//TODO
	}
}
