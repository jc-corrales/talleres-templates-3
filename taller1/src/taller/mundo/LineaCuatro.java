package taller.mundo;

import java.util.ArrayList;
import java.util.Random;

public class LineaCuatro 
{
	/**
	 * Constante que determina a los jugadores de la máquina.
	 */
	public final static String COMPUTADORA = "Com";
	/**
	 * Constante que representa una casilla vacía.
	 */
	public final static String ELEMENTONEUTRO = "(_)";
	//	"___"
	/**
	 * Matriz que representa el estado actual del juego.
	 */
	private Casilla[][] tablero;

	/**
	 * Lista que contiene a los jugadores
	 */
	private ArrayList<Jugador> jugadores;

	/**
	 * Nombre del jugador en turno
	 */
	private String atacante;

	/**
	 * Determina si el juego se terminó
	 */
	private boolean finJuego;

	/**
	 * El número del jugador en turno
	 */
	private int turno;

	/**
	 * Número de filas
	 */
	private int filas;
	/**
	 * Número de columnas
	 */
	private int columnas;
	/**
	 * Cantidad total de fichas que deben estar en fila para que un jugador gane.
	 */
	private int condicionVictoria;
	/**
	 * Crea una nueva instancia del juego
	 */
	public LineaCuatro(ArrayList <Jugador> pJugadores,int pFil, int pCol, int objetivo)
	{
		filas = pFil;
		columnas = pCol;
		tablero = new Casilla[filas][columnas];
		condicionVictoria = objetivo;
		for(int i=0;i<tablero.length;i++)
		{
			for(int j=0;j<tablero[0].length;j++)
			{
				tablero[i][j]= new Casilla(ELEMENTONEUTRO);
			}

		}
		jugadores = pJugadores;
		finJuego = false;
		turno = 0;
		atacante = jugadores.get(turno).darNombre();
	}
	/**
	 * Retorna el tablero del juego
	 * @return tablero
	 */
	public Casilla[][] darTablero()
	{
		return tablero;
	}

	/**
	 * Retorna el jugador en turno
	 * @return atacante jugador en turno
	 */
	public String darAtacante()
	{
		return atacante;
	}
	/**
	 * Método que retorna el Turno.
	 * @return número de turno actual
	 */
	public int darTurno()
	{
		return turno;
	}
	/**
	 * Número de columnas del tablero
	 * @return Columnas del tablero.
	 */
	public int darColumnas()
	{
		return columnas;
	}
	/**
	 * Número de filas del tablero.
	 * @return Filas del tablero.
	 */
	public int darFilas()
	{
		return filas;
	}
	/**
	 * Determina si el juego termina
	 * @return true si el juego se termino, false de lo contrario
	 */
	public boolean fin()
	{
		return finJuego;
	}
	/**
	 * Método que cambia el turno.
	 * @param turno Número de turno.
	 */
	public void cambiarTurno(int nuevoTurno)
	{
		turno = nuevoTurno;
	}
	/**
	 * Registra una jugada aleatoria
	 */
	public void registrarJugadaAleatoria()
	{

		boolean estado = false;
		Casilla fichaNueva = new Casilla(jugadores.get(turno).darSimbolo());
		int contadorEspaciosOcupados = 0;
		Random generador = new Random();
		int col = generador.nextInt(columnas);
		System.out.println(col+1);
		for(int i = filas-1;(i < filas)&&(i >= 0)&&(estado == false); i--)
		{
			System.out.println("Entro en For");
			if (!tablero[i][col].darSimbolo().equals(ELEMENTONEUTRO))
			{
				contadorEspaciosOcupados++;
//				System.out.println("if");
			}
			else if(contadorEspaciosOcupados == filas)
			{
				estado = false;
//				System.out.println("else if");
				col = generador.nextInt(columnas-1);
				return;
			}
			else	
			{
//				System.out.println("ELSE");
				try
				{
					tablero[i][col] = fichaNueva;
					if(i == filas-1 && i == columnas-1)
					{
						;
					}
					else if(i == filas-1)
					{
						if(tablero[i][col+1] != null&&(tablero[i][col+1].darSimbolo().equals(fichaNueva.darSimbolo())))
						{
							fichaNueva.cambiarDerecha(tablero[i][col+1]);
						}
						if(tablero[i-1][col+1] != null&&(tablero[i-1][col+1].darSimbolo().equals(fichaNueva.darSimbolo())))
						{
							fichaNueva.cambiarSupDerecha(tablero[i-1][col+1]);
						}
					}
					else if(i == columnas-1)
					{
						if(tablero[i+1][col] != null&&(tablero[i+1][col].darSimbolo().equals(fichaNueva.darSimbolo())))
						{
							fichaNueva.cambiarInferior(tablero[i+1][col]);
						}
					}
					else if(filas == 0)
					{
						if(tablero[i][col+1] != null&&(tablero[i][col+1].darSimbolo().equals(fichaNueva.darSimbolo())))
						{
							fichaNueva.cambiarDerecha(tablero[i][col+1]);
						}
						if(tablero[i+1][col] != null&&(tablero[i+1][col].darSimbolo().equals(fichaNueva.darSimbolo())))
						{
							fichaNueva.cambiarInferior(tablero[i+1][col]);
						}
						if(tablero[i+1][col+1] != null&&(tablero[i+1][col+1].darSimbolo().equals(fichaNueva.darSimbolo())))
						{
							fichaNueva.cambiarInfDerecha(tablero[i+1][col+1]);
						}
					}
					else
					{
						if(tablero[i][col+1] != null&&(tablero[i][col+1].darSimbolo().equals(fichaNueva.darSimbolo())))
						{
							fichaNueva.cambiarDerecha(tablero[i][col+1]);
						}
						if(tablero[i+1][col] != null&&(tablero[i+1][col].darSimbolo().equals(fichaNueva.darSimbolo())))
						{
							fichaNueva.cambiarInferior(tablero[i+1][col]);
						}
						if(tablero[i+1][col+1] != null&&(tablero[i+1][col+1].darSimbolo().equals(fichaNueva.darSimbolo())))
						{
							fichaNueva.cambiarInfDerecha(tablero[i+1][col+1]);
						}
						if(tablero[i-1][col+1] != null&&(tablero[i-1][col+1].darSimbolo().equals(fichaNueva.darSimbolo())))
						{
							fichaNueva.cambiarSupDerecha(tablero[i-1][col+1]);
						}
					}
				}
				catch (Exception e)
				{
					System.out.println("Algo esta fuera de la matriz " + e.getMessage() + " fila del problema: " + i);
					continue;
				}
				if(turno >= jugadores.size()-1)
				{
					turno = 0;
				}
				else
				{
					turno++;
				}
				atacante = jugadores.get(turno).darNombre();
				estado = true;
				return;
			}
		}
		return;
		//TODO
	}

	/**
	 * Registra una jugada en el tablero
	 * @return true si la jugada se pudo realizar, false de lo contrario
	 */
	public boolean registrarJugada(int col)
	{
		boolean estado = false;
		Casilla fichaNueva = new Casilla(jugadores.get(turno).darSimbolo());
		int contadorEspaciosOcupados = 0;
		for(int i = filas-1;(i<filas)&&(i >= 0)&&(estado == false); i--)
		{
//			System.out.println("Entro en For");
			if (!tablero[i][col].darSimbolo().equals(ELEMENTONEUTRO))
			{
				contadorEspaciosOcupados++;
//				System.out.println("if");
			}
			else if(contadorEspaciosOcupados == filas)
			{
				estado = false;
//				System.out.println("else if");
				System.out.println("Columna " + (col+1) + " llena, intente otra.");
				return estado;
			}
			else	
			{
//				System.out.println("ELSE");
				try
				{
					tablero[i][col] = fichaNueva;
					if(i == filas-1 && i == columnas-1)
					{
						;
					}
					else if(i == filas-1)
					{
						if(tablero[i][col+1] != null&&(tablero[i][col+1].darSimbolo().equals(fichaNueva.darSimbolo())))
						{
							fichaNueva.cambiarDerecha(tablero[i][col+1]);
						}
						if(tablero[i-1][col+1] != null&&(tablero[i-1][col+1].darSimbolo().equals(fichaNueva.darSimbolo())))
						{
							fichaNueva.cambiarSupDerecha(tablero[i-1][col+1]);
						}
					}
					else if(i == columnas-1)
					{
						if(tablero[i+1][col] != null&&(tablero[i+1][col].darSimbolo().equals(fichaNueva.darSimbolo())))
						{
							fichaNueva.cambiarInferior(tablero[i+1][col]);
						}
					}
					else if(filas == 0)
					{
						if(tablero[i][col+1] != null&&(tablero[i][col+1].darSimbolo().equals(fichaNueva.darSimbolo())))
						{
							fichaNueva.cambiarDerecha(tablero[i][col+1]);
						}
						if(tablero[i+1][col] != null&&(tablero[i+1][col].darSimbolo().equals(fichaNueva.darSimbolo())))
						{
							fichaNueva.cambiarInferior(tablero[i+1][col]);
						}
						if(tablero[i+1][col+1] != null&&(tablero[i+1][col+1].darSimbolo().equals(fichaNueva.darSimbolo())))
						{
							fichaNueva.cambiarInfDerecha(tablero[i+1][col+1]);
						}
					}
					else
					{
						if(tablero[i][col+1] != null&&(tablero[i][col+1].darSimbolo().equals(fichaNueva.darSimbolo())))
						{
							fichaNueva.cambiarDerecha(tablero[i][col+1]);
						}
						if(tablero[i+1][col] != null&&(tablero[i+1][col].darSimbolo().equals(fichaNueva.darSimbolo())))
						{
							fichaNueva.cambiarInferior(tablero[i+1][col]);
						}
						if(tablero[i+1][col+1] != null&&(tablero[i+1][col+1].darSimbolo().equals(fichaNueva.darSimbolo())))
						{
							fichaNueva.cambiarInfDerecha(tablero[i+1][col+1]);
						}
						if(tablero[i-1][col+1] != null&&(tablero[i-1][col+1].darSimbolo().equals(fichaNueva.darSimbolo())))
						{
							fichaNueva.cambiarSupDerecha(tablero[i-1][col+1]);
						}
					}
				}
				catch (Exception e)
				{
					System.out.println("Algo esta fuera de la matriz " + e.getMessage() + " fila del problema: " + i);
					continue;
				}
				if(turno >= jugadores.size()-1)
				{
					turno = 0;
				}
				else
				{
					turno++;
				}
				atacante = jugadores.get(turno).darNombre();
				estado = true;
				return estado;
			}
		}
		return estado;

		//		if (turno == jugadores.size())
		//		{
		//			turno = 0;
		//		}
		//		String fichaNueva = jugadores.get(turno).darSimbolo();
		//
		//		for(int i = filas; i > 0; i--)
		//		{
		//			if(tablero[i][col] != null)
		//			{
		//				tablero[i][col] = fichaNueva;
		//				turno++;
		//
		//				return true;
		//			}
		//		}
		//
		//		//TODO
		//
		//		turno++;
		//		return true;
	}

	/**
	 * Determina si una jugada causa el fin del juego
	 * @return true si la jugada termina el juego false de lo contrario
	 */
	//TODO Método sin terminar, los dos algoritmos fracasaron.
	
	public boolean terminar()
	{
				boolean estado = false;
				for(int x = 0; x < columnas; x++)
				{
					for(int y = 0; y < filas;y++)
					{
						System.out.println("Entro en for 2");
						if(!tablero[y][x].darSimbolo().equals(ELEMENTONEUTRO))
						{
							System.out.println("Entro en if");
							int contador = 0;
							//horizontal
							if(tablero[y][x].darDerecha()!=null)
							{
								boolean mismo = true;
								while(mismo)
								{
									if(tablero[y][x].darSimbolo().equals(tablero[y][x+contador].darSimbolo()))
									{
										contador++;
										System.out.println(contador);
									}
									else
									{
										mismo = false;
									}
								}
								if(contador >= condicionVictoria)
								{
									estado = true;
									finJuego = true;
									return true;
								}
							}

							
							while(tablero[y][x].darDerecha()!=null)
							{
								
								contador++;
							}
							if(contador >= condicionVictoria)
							{
								estado = true;
								finJuego = true;
								return true;
							}
							contador = 0;
							//Vertical
							while(tablero[y][x].darInferior()!=null)
							{
								contador++;
							}
							if(contador >= condicionVictoria)
							{
								estado = true;
								finJuego = true;
								return true;
							}
							contador = 0;
							//SupDerecha
							while(tablero[y][x].darSupDerecha()!=null)
							{
								contador++;
							}
							if(contador >= condicionVictoria)
							{
								estado = true;
								finJuego = true;
								return true;
							}
							contador = 0;
							//InfDerecha
							while(tablero[y][x].darInfDerecha()!=null)
							{
								contador++;
							}
							if(contador >= condicionVictoria)
							{
								estado = true;
								finJuego = true;
								return true;
							}
						}
					}
				}
				return estado;

//		//Ciclos que revisan que haya alguna fila.
//		int contadorFila = 0;
//		boolean estado = false;
//		//Revisión de columnas
//		for(int i = 0; i<jugadores.size(); i++)
//		{
//			String simbolo = jugadores.get(i).darSimbolo();
//			for(int x = 0; x < columnas; x++)
//			{
//				for (int y = 0; y < filas; y++)
//				{
//					if(tablero[y][x].equals(simbolo))
//					{
//						contadorFila++;
//					}
//					else
//					{
//						contadorFila = 0;
//					}
//					if (contadorFila >= 4 )
//					{
//						System.out.println("El jugador: " + jugadores.get(i).darNombre()+ " es el Ganador.");
//						estado = true;
//						return estado;
//					}
//				}
//			}
//			if (contadorFila >= 4 )
//			{
//				System.out.println("El jugador: " + jugadores.get(i).darNombre()+ " es el Ganador.");
//				estado = true;
//				return estado;
//			}
//		}
//		//Revisión de las Filas.
//		int contadorColumna = 0;
//		for(int i = 0; i<jugadores.size(); i++)
//		{
//			String simbolo = jugadores.get(i).darSimbolo();
//			for(int y = 0; y < filas; y++)
//			{
//				for (int x = 0; x < columnas; x++)
//				{
//					if(tablero[y][x].equals(simbolo))
//					{
//						contadorColumna++;
//					}
//					else
//					{
//						contadorColumna = 0;
//					}
//					if (contadorColumna >= 4 )
//					{
//						System.out.println("El jugador: " + jugadores.get(i).darNombre()+ " es el Ganador.");
//						estado = true;
//						return estado;
//					}
//				}
//			}
//			if (contadorColumna >= 4 )
//			{
//				System.out.println("El jugador: " + jugadores.get(i).darNombre()+ " es el Ganador.");
//				estado = true;
//				return estado;
//			}
//		}
//		//Revisión de diagonales derecha inferior de la casilla actual.
//		for(int i = 0; i<jugadores.size(); i++)
//		{
//			String simbolo = jugadores.get(i).darSimbolo();
//			for(int x = 0; x < columnas; x++)
//			{
//				for (int y = 0; y < filas; y++)
//				{
//					int extraX = 0;
//					int extraY = 0;
//					while((extraX < 4) && (x + extraX < columnas))
//					{
//						while (extraY < 4  && (y + extraY < filas))
//						{
//							if(tablero[y + extraY ][x + extraX].equals(simbolo))
//							{
//								contadorColumna++;
//							}
//							else
//							{
//								contadorColumna = 0;
//							}
//							if (contadorFila >= 4 )
//							{
//								System.out.println("El jugador: " + jugadores.get(i).darNombre()+ " es el Ganador.");
//								estado = true;
//								return estado;
//							}
//							extraY++;
//						}
//
//						extraX++;
//					}
//				}
//			}
//
//		}
//		return estado;
		//TODO

	}


}
