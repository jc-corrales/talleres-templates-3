package taller.mundo;

/**
 * Clase que representa una casilla de un tablero.
 * @author JuanCarlos
 *
 */
public class Casilla
{
	//Atributos
	/**
	 * Atributo que contiene el símbolo. 
	 */
	private String simbolo;
	/**
	 * Atributo que referencia la casilla inferior.
	 */
	private Casilla inferior;
	/**
	 * Atributo que referencia la siguiente casilla.
	 */
	private Casilla derecha;
	/**
	 * Atributo que referencia la casilla inferior derecha.
	 */
	private Casilla infDerecha;
	/**
	 * Atributo que referencia la casilla superior derecha.
	 */
	private Casilla supDerecha;
	/**
	 * Constructor de la clase, recibe una cadena por parámetro, el símbolo de un jugador.
	 * @param symb Símbolo de un jugador.
	 */
	public Casilla (String symb)
	{
		simbolo = symb;
	}
	//Métodos de lectura
	/**
	 * Método que retorna el simbolo.
	 * @return
	 */
	public String darSimbolo()
	{
		return simbolo;
	}
	/**
	 * Método que retorna la siguiente casilla.
	 * @return Casilla
	 */
	public Casilla darDerecha()
	{
		return derecha;
	}
	/**
	 * Método que retorna la casilla inferior.
	 * @return Casilla
	 */
	public Casilla darInferior()
	{
		return inferior;
	}
	/**
	 * Método que retorna la casilla superior derecha.
	 * @return Casilla
	 */
	public Casilla darSupDerecha()
	{
		return supDerecha;
	}
	/**
	 * Método que retorna la casilla inferior derecha.
	 * @return Casilla
	 */
	public Casilla darInfDerecha()
	{
		return infDerecha;
	}
	//Métodos de modificación
	/**
	 * Método para cambiar la referencia de la casilla derecha.
	 * @param nuevaDerecha
	 */
	public void cambiarDerecha(Casilla nuevaDerecha)
	{
		derecha = nuevaDerecha;
	}
	/**
	 * Método para cambiar la referencia de la casilla inferior
	 * @param nuevaInferior
	 */
	public void cambiarInferior(Casilla nuevaInferior)
	{
		inferior = nuevaInferior;
	}
	/**
	 * Método para cambiar la referencia de la casilla inferior derecha.
	 * @param nuevaInfDerecha
	 */
	public void cambiarInfDerecha(Casilla nuevaInfDerecha)
	{
		infDerecha = nuevaInfDerecha;
	}
	/**
	 * Método para cambiar la referencia de la casilla superior Derecha.
	 * @param nuevaSupDerecha
	 */
	public void cambiarSupDerecha(Casilla nuevaSupDerecha)
	{
		supDerecha = nuevaSupDerecha;
	}
}
